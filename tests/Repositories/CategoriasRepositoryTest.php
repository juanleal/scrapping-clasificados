<?php namespace Tests\Repositories;

use App\Models\Categorias;
use App\Repositories\CategoriasRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class CategoriasRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var CategoriasRepository
     */
    protected $categoriasRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->categoriasRepo = \App::make(CategoriasRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_categorias()
    {
        $categorias = Categorias::factory()->make()->toArray();

        $createdCategorias = $this->categoriasRepo->create($categorias);

        $createdCategorias = $createdCategorias->toArray();
        $this->assertArrayHasKey('id', $createdCategorias);
        $this->assertNotNull($createdCategorias['id'], 'Created Categorias must have id specified');
        $this->assertNotNull(Categorias::find($createdCategorias['id']), 'Categorias with given id must be in DB');
        $this->assertModelData($categorias, $createdCategorias);
    }

    /**
     * @test read
     */
    public function test_read_categorias()
    {
        $categorias = Categorias::factory()->create();

        $dbCategorias = $this->categoriasRepo->find($categorias->id);

        $dbCategorias = $dbCategorias->toArray();
        $this->assertModelData($categorias->toArray(), $dbCategorias);
    }

    /**
     * @test update
     */
    public function test_update_categorias()
    {
        $categorias = Categorias::factory()->create();
        $fakeCategorias = Categorias::factory()->make()->toArray();

        $updatedCategorias = $this->categoriasRepo->update($fakeCategorias, $categorias->id);

        $this->assertModelData($fakeCategorias, $updatedCategorias->toArray());
        $dbCategorias = $this->categoriasRepo->find($categorias->id);
        $this->assertModelData($fakeCategorias, $dbCategorias->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_categorias()
    {
        $categorias = Categorias::factory()->create();

        $resp = $this->categoriasRepo->delete($categorias->id);

        $this->assertTrue($resp);
        $this->assertNull(Categorias::find($categorias->id), 'Categorias should not exist in DB');
    }
}
