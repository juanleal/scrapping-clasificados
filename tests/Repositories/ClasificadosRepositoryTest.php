<?php namespace Tests\Repositories;

use App\Models\Clasificados;
use App\Repositories\ClasificadosRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ClasificadosRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ClasificadosRepository
     */
    protected $clasificadosRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->clasificadosRepo = \App::make(ClasificadosRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_clasificados()
    {
        $clasificados = Clasificados::factory()->make()->toArray();

        $createdClasificados = $this->clasificadosRepo->create($clasificados);

        $createdClasificados = $createdClasificados->toArray();
        $this->assertArrayHasKey('id', $createdClasificados);
        $this->assertNotNull($createdClasificados['id'], 'Created Clasificados must have id specified');
        $this->assertNotNull(Clasificados::find($createdClasificados['id']), 'Clasificados with given id must be in DB');
        $this->assertModelData($clasificados, $createdClasificados);
    }

    /**
     * @test read
     */
    public function test_read_clasificados()
    {
        $clasificados = Clasificados::factory()->create();

        $dbClasificados = $this->clasificadosRepo->find($clasificados->id);

        $dbClasificados = $dbClasificados->toArray();
        $this->assertModelData($clasificados->toArray(), $dbClasificados);
    }

    /**
     * @test update
     */
    public function test_update_clasificados()
    {
        $clasificados = Clasificados::factory()->create();
        $fakeClasificados = Clasificados::factory()->make()->toArray();

        $updatedClasificados = $this->clasificadosRepo->update($fakeClasificados, $clasificados->id);

        $this->assertModelData($fakeClasificados, $updatedClasificados->toArray());
        $dbClasificados = $this->clasificadosRepo->find($clasificados->id);
        $this->assertModelData($fakeClasificados, $dbClasificados->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_clasificados()
    {
        $clasificados = Clasificados::factory()->create();

        $resp = $this->clasificadosRepo->delete($clasificados->id);

        $this->assertTrue($resp);
        $this->assertNull(Clasificados::find($clasificados->id), 'Clasificados should not exist in DB');
    }
}
