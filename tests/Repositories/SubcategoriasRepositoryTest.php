<?php namespace Tests\Repositories;

use App\Models\Subcategorias;
use App\Repositories\SubcategoriasRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SubcategoriasRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SubcategoriasRepository
     */
    protected $subcategoriasRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->subcategoriasRepo = \App::make(SubcategoriasRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_subcategorias()
    {
        $subcategorias = Subcategorias::factory()->make()->toArray();

        $createdSubcategorias = $this->subcategoriasRepo->create($subcategorias);

        $createdSubcategorias = $createdSubcategorias->toArray();
        $this->assertArrayHasKey('id', $createdSubcategorias);
        $this->assertNotNull($createdSubcategorias['id'], 'Created Subcategorias must have id specified');
        $this->assertNotNull(Subcategorias::find($createdSubcategorias['id']), 'Subcategorias with given id must be in DB');
        $this->assertModelData($subcategorias, $createdSubcategorias);
    }

    /**
     * @test read
     */
    public function test_read_subcategorias()
    {
        $subcategorias = Subcategorias::factory()->create();

        $dbSubcategorias = $this->subcategoriasRepo->find($subcategorias->id);

        $dbSubcategorias = $dbSubcategorias->toArray();
        $this->assertModelData($subcategorias->toArray(), $dbSubcategorias);
    }

    /**
     * @test update
     */
    public function test_update_subcategorias()
    {
        $subcategorias = Subcategorias::factory()->create();
        $fakeSubcategorias = Subcategorias::factory()->make()->toArray();

        $updatedSubcategorias = $this->subcategoriasRepo->update($fakeSubcategorias, $subcategorias->id);

        $this->assertModelData($fakeSubcategorias, $updatedSubcategorias->toArray());
        $dbSubcategorias = $this->subcategoriasRepo->find($subcategorias->id);
        $this->assertModelData($fakeSubcategorias, $dbSubcategorias->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_subcategorias()
    {
        $subcategorias = Subcategorias::factory()->create();

        $resp = $this->subcategoriasRepo->delete($subcategorias->id);

        $this->assertTrue($resp);
        $this->assertNull(Subcategorias::find($subcategorias->id), 'Subcategorias should not exist in DB');
    }
}
