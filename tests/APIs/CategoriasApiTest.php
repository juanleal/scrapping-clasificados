<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Categorias;

class CategoriasApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_categorias()
    {
        $categorias = Categorias::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/categorias', $categorias
        );

        $this->assertApiResponse($categorias);
    }

    /**
     * @test
     */
    public function test_read_categorias()
    {
        $categorias = Categorias::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/categorias/'.$categorias->id
        );

        $this->assertApiResponse($categorias->toArray());
    }

    /**
     * @test
     */
    public function test_update_categorias()
    {
        $categorias = Categorias::factory()->create();
        $editedCategorias = Categorias::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/categorias/'.$categorias->id,
            $editedCategorias
        );

        $this->assertApiResponse($editedCategorias);
    }

    /**
     * @test
     */
    public function test_delete_categorias()
    {
        $categorias = Categorias::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/categorias/'.$categorias->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/categorias/'.$categorias->id
        );

        $this->response->assertStatus(404);
    }
}
