<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Subcategorias;

class SubcategoriasApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_subcategorias()
    {
        $subcategorias = Subcategorias::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/subcategorias', $subcategorias
        );

        $this->assertApiResponse($subcategorias);
    }

    /**
     * @test
     */
    public function test_read_subcategorias()
    {
        $subcategorias = Subcategorias::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/subcategorias/'.$subcategorias->id
        );

        $this->assertApiResponse($subcategorias->toArray());
    }

    /**
     * @test
     */
    public function test_update_subcategorias()
    {
        $subcategorias = Subcategorias::factory()->create();
        $editedSubcategorias = Subcategorias::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/subcategorias/'.$subcategorias->id,
            $editedSubcategorias
        );

        $this->assertApiResponse($editedSubcategorias);
    }

    /**
     * @test
     */
    public function test_delete_subcategorias()
    {
        $subcategorias = Subcategorias::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/subcategorias/'.$subcategorias->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/subcategorias/'.$subcategorias->id
        );

        $this->response->assertStatus(404);
    }
}
