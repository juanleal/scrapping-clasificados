<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Clasificados;

class ClasificadosApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_clasificados()
    {
        $clasificados = Clasificados::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/clasificados', $clasificados
        );

        $this->assertApiResponse($clasificados);
    }

    /**
     * @test
     */
    public function test_read_clasificados()
    {
        $clasificados = Clasificados::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/clasificados/'.$clasificados->id
        );

        $this->assertApiResponse($clasificados->toArray());
    }

    /**
     * @test
     */
    public function test_update_clasificados()
    {
        $clasificados = Clasificados::factory()->create();
        $editedClasificados = Clasificados::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/clasificados/'.$clasificados->id,
            $editedClasificados
        );

        $this->assertApiResponse($editedClasificados);
    }

    /**
     * @test
     */
    public function test_delete_clasificados()
    {
        $clasificados = Clasificados::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/clasificados/'.$clasificados->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/clasificados/'.$clasificados->id
        );

        $this->response->assertStatus(404);
    }
}
