<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Nombre:') !!}
    <p>{{ $subcategorias->name }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Descripción:') !!}
    <p>{{ $subcategorias->description }}</p>
</div>

<!-- Main filter Field -->
<div class="form-group">
    {!! Form::label('main_filter', 'Main filter:') !!}
    <p>{{ $subcategorias->main_filter }}</p>
</div>

<!-- filters Field -->
<div class="form-group">
    {!! Form::label('filters', 'Filters:') !!}
    <p>{{ $subcategorias->filters }}</p>
</div>

<!-- Categorias Id Field -->
<div class="form-group">
    {!! Form::label('categorias_id', 'Categoría:') !!}
    <p>{{ $subcategorias->categorias->description }}</p>
</div>

