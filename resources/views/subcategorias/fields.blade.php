@inject('categoriasctrl', 'App\Http\Controllers\CategoriasController')

<!-- Name Field -->
<div class="form-group col-sm-4">
    {!! Form::label('name', 'Nombre:') !!}
    {!! Form::text('name', null, ['class' => 'form-control','maxlength' => 50,'maxlength' => 50]) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-4">
    {!! Form::label('description', 'Descripción:') !!}
    {!! Form::text('description', null, ['class' => 'form-control','maxlength' => 50,'maxlength' => 50]) !!}
</div>

<!-- Categorias Id Field -->
<div class="form-group col-sm-4">
    {!! Form::label('categorias_id', 'Categoría:') !!}
    {!! Form::select('categorias_id', $categoriasctrl->list(), null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Seleccione...']) !!}
    {{--{!! Form::number('categorias_id', null, ['class' => 'form-control']) !!}--}}
</div>

<!-- Main filter Field -->
<div class="form-group col-sm-6">
    {!! Form::label('main_filter', 'Main filter:') !!}
    {!! Form::text('main_filter', null, ['class' => 'form-control']) !!}
</div>

<!-- Filters Field -->
<div class="form-group col-sm-6">
    {!! Form::label('filters', 'Filters:') !!}
    {!! Form::text('filters', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('subcategorias.index') }}" class="btn btn-default">Cancel</a>
</div>
