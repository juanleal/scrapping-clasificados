<div class="table-responsive">
    <table class="table" id="subcategorias-table">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Descripción</th>
                <th>Categoría</th>
                <th colspan="3">Acción</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($subcategorias as $subcategorias)
                <tr>
                    <td>{{ $subcategorias->name }}</td>
                    <td>{{ $subcategorias->description }}</td>
                    <td>{{ $subcategorias->categorias->description }}</td>
                    <td>
                        {!! Form::open(['route' => ['subcategorias.destroy', $subcategorias->id], 'method' => 'delete'])
                        !!}
                        <div class='btn-group'>
                            <a href="{{ route('subcategorias.show', [$subcategorias->id]) }}"
                                class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                            <a href="{{ route('subcategorias.edit', [$subcategorias->id]) }}"
                                class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class'
                            => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
