<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nombre:') !!}
    {!! Form::text('name', null, ['class' => 'form-control','maxlength' => 50,'maxlength' => 50]) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Descripción:') !!}
    {!! Form::text('description', null, ['class' => 'form-control','maxlength' => 50,'maxlength' => 50]) !!}
</div>

<!-- Main filter Field -->
<div class="form-group col-sm-6">
    {!! Form::label('main_filter', 'Main filter:') !!}
    {!! Form::text('main_filter', null, ['class' => 'form-control']) !!}
</div>

<!-- Filters Field -->
<div class="form-group col-sm-6">
    {!! Form::label('filters', 'Filters:') !!}
    {!! Form::text('filters', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('categorias.index') }}" class="btn btn-default">Cancel</a>
</div>
