<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Nombre:') !!}
    <p>{{ $categorias->name }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Descripción:') !!}
    <p>{{ $categorias->description }}</p>
</div>

<!-- Main filter Field -->
<div class="form-group">
    {!! Form::label('main_filter', 'Main filter:') !!}
    <p>{{ $categorias->main_filter }}</p>
</div>

<!-- filters Field -->
<div class="form-group">
    {!! Form::label('filters', 'Filters:') !!}
    <p>{{ $categorias->filters }}</p>
</div>
