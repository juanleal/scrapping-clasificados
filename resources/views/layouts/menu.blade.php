<li class="{{ Request::is('home') ? 'active' : '' }}">
    <a href="{{ route('home') }}"><i class="fa fa-home"></i><span>Anuncios</span></a>
</li>
<li class="{{ Request::is('categorias*') ? 'active' : '' }}">
    <a href="{{ route('categorias.index') }}"><i class="fa fa-edit"></i><span>Categorias</span></a>
</li>

<li class="{{ Request::is('subcategorias*') ? 'active' : '' }}">
    <a href="{{ route('subcategorias.index') }}"><i class="fa fa-edit"></i><span>Subcategorias</span></a>
</li>

<li class="{{ Request::is('clasificados*') ? 'active' : '' }}">
    <a href="{{ route('clasificados.index') }}"><i class="fa fa-edit"></i><span>Anuncios guardados</span></a>
</li>

