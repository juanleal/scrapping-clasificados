<div class="table-responsive">
    <table class="table" id="clasificados-table">
        <thead>
            <tr>
                <th>Ad Id</th>
                <th>Title</th>
                <th>Content</th>
                <th>Image Url</th>
                <th>Link</th>
                <th>Categoría</th>
                <th>Subcategoría</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($clasificados as $clasificados)
                <tr>
                    <td>{{ $clasificados->ad_id }}</td>
                    <td>{{ $clasificados->title }}</td>
                    <td>{{ \Illuminate\Support\Str::limit($clasificados->content, 100, '...') }}</td>
                    <td>{{ $clasificados->image_url }}</td>
                    <td>{{ $clasificados->link }}</td>
                    <td>{{ isset($clasificados->categorias) ? $clasificados->categorias->description : null }}</td>
                    <td>{{ isset($clasificados->subcategorias) ? $clasificados->subcategorias->description : null }}</td>
                    <td>
                        {!! Form::open(['route' => ['clasificados.destroy', $clasificados->id], 'method' => 'delete'])
                        !!}
                        <div class='btn-group'>
                            <a href="{{ route('clasificados.show', [$clasificados->id]) }}"
                                class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                            <a href="{{ route('clasificados.edit', [$clasificados->id]) }}"
                                class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class'
                            => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
