<!-- Ad Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ad_id', 'Ad Id:') !!}
    {!! Form::text('ad_id', null, ['class' => 'form-control','maxlength' => 50,'maxlength' => 50,'maxlength' => 50]) !!}
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control','maxlength' => 50,'maxlength' => 50,'maxlength' => 50]) !!}
</div>

<!-- Content Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('content', 'Content:') !!}
    {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Url Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('image_url', 'Image Url:') !!}
    {!! Form::textarea('image_url', null, ['class' => 'form-control']) !!}
</div>

<!-- Link Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('link', 'Link:') !!}
    {!! Form::textarea('link', null, ['class' => 'form-control']) !!}
</div>

<!-- Categorias Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('categorias_id', 'Categorias Id:') !!}
    {!! Form::number('categorias_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Subcategorias Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('subcategorias_id', 'Subcategorias Id:') !!}
    {!! Form::number('subcategorias_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('clasificados.index') }}" class="btn btn-default">Cancel</a>
</div>
