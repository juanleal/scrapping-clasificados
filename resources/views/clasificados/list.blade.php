@if (isset($clasificados['title']))
    @foreach ($clasificados['title'] as $key => $clasificado)
        <div class="row">
            <div class="col-md-10">
                <div class="panel panel-default  panel--styled">
                    <div class="panel-body">
                        <div class="col-md-12 panelTop">
                            <div class="col-md-4">
                                <img class="img-responsive"
                                    src="{{ isset($clasificados['image_url'][$key]) ? $clasificados['image_url'][$key] : '' }}"
                                    alt="" />
                            </div>
                            <div class="col-md-8">
                                <h2>{{ $clasificados['title'][$key] }}</h2>
                                <p>{{ $clasificados['content'][$key] }}</p>
                            </div>
                        </div>

                        <div class="col-md-12 panelBottom">
                            <div class="col-md-3 pull-left">
                                <button_comment :ad_id="'{{ $clasificados['ad_id'][$key] }}'" :key_id="{{ $key }}">
                                </button_comment>
                            </div>
                            <div class="col-md-6 text-center">
                                <a class="btn btn-sm btn-info" href="{{ $clasificados['link'][$key] }}"
                                    target="_blank"><span class="glyphicon glyphicon-info-sign"></span> Ver más</a>
                            </div>
                            <div class="col-md-3 text-center pull-right social-buttons">
                                <a href="https://www.facebook.com/sharer/sharer.php?u={{ $clasificados['link'][$key] }}"
                                    target="_blank">
                                    <i class="fa fa-facebook-official"></i>
                                </a>
                                <a href="https://twitter.com/intent/tweet?url={{ $clasificados['link'][$key] }}"
                                   target="_blank">
                                    <i class="fa fa-twitter-square"></i>
                                </a>
                            </div>
                        </div>
                        <div class="collapse col-md-12 box-comments" id="comments-{{ $key }}">
                            <div class="well">
                                <comments :comments_wrapper_classes="['custom-scrollbar', 'comments-wrapper']"
                                    :comments="[]" :current_user="{{ Auth::user() }}"
                                    :ad_id="'{{ $clasificados['ad_id'][$key] }}'">
                                </comments>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@else
    @if (request()->get('categorias_id'))
        <div class="row">
            <div class="col-md-10 text-center">
                <h3>No se obtuvieron resultados</h3>
            </div>
        </div>
    @endif
@endif
@push('scripts')
    <script>
        var popupSize = {
            width: 780,
            height: 550
        };
        $(document).on('click', '.social-buttons > a', function(e) {

            var
                verticalPos = Math.floor(($(window).width() - popupSize.width) / 2),
                horisontalPos = Math.floor(($(window).height() - popupSize.height) / 2);

            var popup = window.open($(this).prop('href'), 'social',
                'width=' + popupSize.width + ',height=' + popupSize.height +
                ',left=' + verticalPos + ',top=' + horisontalPos +
                ',location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1');

            if (popup) {
                popup.focus();
                e.preventDefault();
            }

        });

    </script>
@endpush
