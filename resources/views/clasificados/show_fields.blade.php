<!-- Ad Id Field -->
<div class="form-group">
    {!! Form::label('ad_id', 'Ad Id:') !!}
    <p>{{ $clasificados->ad_id }}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $clasificados->title }}</p>
</div>

<!-- Content Field -->
<div class="form-group">
    {!! Form::label('content', 'Content:') !!}
    <p>{{ $clasificados->content }}</p>
</div>

<!-- Image Url Field -->
<div class="form-group">
    {!! Form::label('image_url', 'Image Url:') !!}
    <p>{{ $clasificados->image_url }}</p>
</div>

<!-- Link Field -->
<div class="form-group">
    {!! Form::label('link', 'Link:') !!}
    <p>{{ $clasificados->link }}</p>
</div>

<!-- Categorias Id Field -->
<div class="form-group">
    {!! Form::label('categorias_id', 'Categorias Id:') !!}
    <p>{{ $clasificados->categorias_id }}</p>
</div>

<!-- Subcategorias Id Field -->
<div class="form-group">
    {!! Form::label('subcategorias_id', 'Subcategorias Id:') !!}
    <p>{{ $clasificados->subcategorias_id }}</p>
</div>

