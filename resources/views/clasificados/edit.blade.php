@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Clasificados
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($clasificados, ['route' => ['clasificados.update', $clasificados->id], 'method' => 'patch']) !!}

                        @include('clasificados.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection