@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            {!! Form::open(['route' => 'home', 'method' => 'GET']) !!}
            <categories_filter defaultcategoria="{{request()->get('categorias_id')}}" defaultsubcategoria="{{request()->get('subcategorias_id')}}"></categories_filter>
            <div class="col-md-4">
                <div class="form-group col-sm-12">
                    {!! Form::submit('Buscar', ['class' => 'btn btn-primary button-filter']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
            @include('clasificados.list')
    </div>
@endsection
