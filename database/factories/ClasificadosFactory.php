<?php

namespace Database\Factories;

use App\Models\Clasificados;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClasificadosFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Clasificados::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'ad_id' => $this->faker->word,
        'title' => $this->faker->word,
        'content' => $this->faker->text,
        'image_url' => $this->faker->text,
        'link' => $this->faker->text,
        'categorias_id' => $this->faker->randomDigitNotNull,
        'subcategorias_id' => $this->faker->randomDigitNotNull
        ];
    }
}
