<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});



Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::resource('categorias', App\Http\Controllers\CategoriasController::class);

Route::resource('subcategorias', App\Http\Controllers\SubcategoriasController::class);

Route::resource('clasificados', App\Http\Controllers\ClasificadosController::class);

Route::resource('comments', App\Http\Controllers\CommentsController::class);