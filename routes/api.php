<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Versión 1.0 del API rest
Route::group([
    'prefix' => 'v1'
], function () {

    Route::post('login', 'v1\AuthController@login');
    Route::post('signup', 'v1\AuthController@signUp');

    Route::middleware(['auth:api'])->group(function () {
        Route::get('logout', 'v1\AuthController@logout');
        Route::get('user', 'v1\AuthController@user');
        
        Route::resource('clientes', v1\ClientesAPIController::class);

    });

    Route::resource('categorias', \v1\CategoriasAPIController::class);        
    Route::resource('subcategorias', \v1\SubcategoriasAPIController::class);
    Route::resource('clasificados', \v1\ClasificadosAPIController::class);
});
