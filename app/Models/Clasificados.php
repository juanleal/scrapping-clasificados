<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @SWG\Definition(
 *      definition="Clasificados",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="ad_id",
 *          description="ad_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="title",
 *          description="title",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="content",
 *          description="content",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="image_url",
 *          description="image_url",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="link",
 *          description="link",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="categorias_id",
 *          description="categorias_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="subcategorias_id",
 *          description="subcategorias_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Clasificados extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'clasificados';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'ad_id',
        'title',
        'content',
        'image_url',
        'link',
        'categorias_id',
        'subcategorias_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'ad_id' => 'string',
        'title' => 'string',
        'content' => 'string',
        'image_url' => 'string',
        'link' => 'string',
        'categorias_id' => 'integer',
        'subcategorias_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'ad_id' => 'nullable|string|max:50',
        'title' => 'nullable|string|max:50',
        'content' => 'nullable|string',
        'image_url' => 'nullable|string',
        'link' => 'nullable|string',
        'categorias_id' => 'nullable|integer',
        'subcategorias_id' => 'nullable|integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function categorias()
    {
        return $this->belongsTo(\App\Models\Categorias::class, 'categorias_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function subcategorias()
    {
        return $this->belongsTo(\App\Models\Subcategorias::class, 'subcategorias_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function comments()
    {
        return $this->hasMany(\App\Models\Comments::class, 'clasificados_ad_id', 'ad_id')->with(['users']);
    }
}
