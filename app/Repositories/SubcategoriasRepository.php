<?php

namespace App\Repositories;

use App\Models\Subcategorias;
use App\Repositories\BaseRepository;

/**
 * Class SubcategoriasRepository
 * @package App\Repositories
 * @version January 9, 2021, 1:01 am UTC
*/

class SubcategoriasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description',
        'categorias_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Subcategorias::class;
    }
}
