<?php

namespace App\Repositories;

use App\Models\Clasificados;
use App\Repositories\BaseRepository;

/**
 * Class ClasificadosRepository
 * @package App\Repositories
 * @version January 9, 2021, 6:14 am UTC
*/

class ClasificadosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'ad_id',
        'title',
        'content',
        'image_url',
        'link',
        'categorias_id',
        'subcategorias_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Clasificados::class;
    }
    
    /**
     * Get facturas with items
     **/
    public function allClasificados($search = [], $skip = null, $limit = null)
    {
        $query = $this->model->newQuery();
        $query->with(['categorias','subcategorias', 'comments'])->whereNull('clasificados.deleted_at');

        if (count($search)) {
            foreach($search as $key => $value) {
                if (in_array($key, $this->getFieldsSearchable())) {
                    $query->where($key, $value);
                }
            }
        }

        if (!is_null($skip)) {
            $query->skip($skip);
        }

        if (!is_null($limit)) {
            $query->limit($limit);
        }

        return $query;
    }
}
