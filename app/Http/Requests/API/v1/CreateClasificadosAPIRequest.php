<?php

namespace App\Http\Requests\API\v1;

use App\Models\Clasificados;
use InfyOm\Generator\Request\APIRequest;

class CreateClasificadosAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Clasificados::$rules;
    }
}
