<?php

namespace App\Http\Requests\API\v1;

use App\Models\Subcategorias;
use InfyOm\Generator\Request\APIRequest;

class UpdateSubcategoriasAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Subcategorias::$rules;
        
        return $rules;
    }
}
