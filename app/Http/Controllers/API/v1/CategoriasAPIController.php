<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Requests\API\v1\CreateCategoriasAPIRequest;
use App\Http\Requests\API\v1\UpdateCategoriasAPIRequest;
use App\Models\Categorias;
use App\Repositories\CategoriasRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\CategoriasResource;
use Response;

/**
 * Class CategoriasController
 * @package App\Http\Controllers\API\v1
 */

class CategoriasAPIController extends AppBaseController
{
    /** @var  CategoriasRepository */
    private $categoriasRepository;

    public function __construct(CategoriasRepository $categoriasRepo)
    {
        $this->categoriasRepository = $categoriasRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/categorias",
     *      summary="Get a listing of the Categorias.",
     *      tags={"Categorias"},
     *      description="Get all Categorias",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Categorias")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $categorias = $this->categoriasRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(CategoriasResource::collection($categorias), 'Categorias retrieved successfully');
    }

    /**
     * @param CreateCategoriasAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/categorias",
     *      summary="Store a newly created Categorias in storage",
     *      tags={"Categorias"},
     *      description="Store Categorias",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Categorias that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Categorias")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Categorias"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateCategoriasAPIRequest $request)
    {
        $input = $request->all();

        $categorias = $this->categoriasRepository->create($input);

        return $this->sendResponse(new CategoriasResource($categorias), 'Categorias saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/categorias/{id}",
     *      summary="Display the specified Categorias",
     *      tags={"Categorias"},
     *      description="Get Categorias",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Categorias",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Categorias"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Categorias $categorias */
        $categorias = $this->categoriasRepository->find($id);

        if (empty($categorias)) {
            return $this->sendError('Categorias not found');
        }

        return $this->sendResponse(new CategoriasResource($categorias), 'Categorias retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateCategoriasAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/categorias/{id}",
     *      summary="Update the specified Categorias in storage",
     *      tags={"Categorias"},
     *      description="Update Categorias",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Categorias",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Categorias that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Categorias")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Categorias"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCategoriasAPIRequest $request)
    {
        $input = $request->all();

        /** @var Categorias $categorias */
        $categorias = $this->categoriasRepository->find($id);

        if (empty($categorias)) {
            return $this->sendError('Categorias not found');
        }

        $categorias = $this->categoriasRepository->update($input, $id);

        return $this->sendResponse(new CategoriasResource($categorias), 'Categorias updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/categorias/{id}",
     *      summary="Remove the specified Categorias from storage",
     *      tags={"Categorias"},
     *      description="Delete Categorias",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Categorias",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Categorias $categorias */
        $categorias = $this->categoriasRepository->find($id);

        if (empty($categorias)) {
            return $this->sendError('Categorias not found');
        }

        $categorias->delete();

        return $this->sendSuccess('Categorias deleted successfully');
    }
}
