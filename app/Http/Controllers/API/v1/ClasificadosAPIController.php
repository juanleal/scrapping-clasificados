<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Requests\API\v1\CreateClasificadosAPIRequest;
use App\Http\Requests\API\v1\UpdateClasificadosAPIRequest;
use App\Models\Clasificados;
use App\Repositories\ClasificadosRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\ClasificadosResource;
use Response;

/**
 * Class ClasificadosController
 * @package App\Http\Controllers\API\v1
 */

class ClasificadosAPIController extends AppBaseController
{
    /** @var  ClasificadosRepository */
    private $clasificadosRepository;

    public function __construct(ClasificadosRepository $clasificadosRepo)
    {
        $this->clasificadosRepository = $clasificadosRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/clasificados",
     *      summary="Get a listing of the Clasificados.",
     *      tags={"Clasificados"},
     *      description="Get all Clasificados",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Clasificados")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $clasificados = $this->clasificadosRepository->allClasificados(
            $request->except(['skip', 'limit', 'title', 'content']),
            $request->get('skip'),
            $request->get('limit')
        );

        if($request->has('title')){
            $clasificados->where('title', 'like', "%{$request->title}%");
        }
        if($request->has('content')){
            $clasificados->where('content', 'like', "%{$request->content}%");
        }

        return $this->sendResponse(ClasificadosResource::collection($clasificados->get()), 'Clasificados retrieved successfully');
    }

    /**
     * @param CreateClasificadosAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/clasificados",
     *      summary="Store a newly created Clasificados in storage",
     *      tags={"Clasificados"},
     *      description="Store Clasificados",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Clasificados that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Clasificados")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Clasificados"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateClasificadosAPIRequest $request)
    {
        $input = $request->all();

        $clasificados = $this->clasificadosRepository->find($request->ad_id);
        if (empty($clasificados)) {
            $clasificados = $this->clasificadosRepository->create($input);
        }

        return $this->sendResponse(new ClasificadosResource($clasificados), 'Clasificados saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/clasificados/{id}",
     *      summary="Display the specified Clasificados",
     *      tags={"Clasificados"},
     *      description="Get Clasificados",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Clasificados",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Clasificados"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Clasificados $clasificados */
        $clasificados = $this->clasificadosRepository->find($id);

        if (empty($clasificados)) {
            return $this->sendError('Clasificados not found');
        }

        return $this->sendResponse(new ClasificadosResource($clasificados), 'Clasificados retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateClasificadosAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/clasificados/{id}",
     *      summary="Update the specified Clasificados in storage",
     *      tags={"Clasificados"},
     *      description="Update Clasificados",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Clasificados",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Clasificados that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Clasificados")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Clasificados"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateClasificadosAPIRequest $request)
    {
        $input = $request->all();

        /** @var Clasificados $clasificados */
        $clasificados = $this->clasificadosRepository->find($id);

        if (empty($clasificados)) {
            return $this->sendError('Clasificados not found');
        }

        $clasificados = $this->clasificadosRepository->update($input, $id);

        return $this->sendResponse(new ClasificadosResource($clasificados), 'Clasificados updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/clasificados/{id}",
     *      summary="Remove the specified Clasificados from storage",
     *      tags={"Clasificados"},
     *      description="Delete Clasificados",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Clasificados",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Clasificados $clasificados */
        $clasificados = $this->clasificadosRepository->find($id);

        if (empty($clasificados)) {
            return $this->sendError('Clasificados not found');
        }

        $clasificados->delete();

        return $this->sendSuccess('Clasificados deleted successfully');
    }
}
