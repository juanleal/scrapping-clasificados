<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Requests\API\v1\CreateSubcategoriasAPIRequest;
use App\Http\Requests\API\v1\UpdateSubcategoriasAPIRequest;
use App\Models\Subcategorias;
use App\Repositories\SubcategoriasRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\SubcategoriasResource;
use Response;

/**
 * Class SubcategoriasController
 * @package App\Http\Controllers\API\v1
 */

class SubcategoriasAPIController extends AppBaseController
{
    /** @var  SubcategoriasRepository */
    private $subcategoriasRepository;

    public function __construct(SubcategoriasRepository $subcategoriasRepo)
    {
        $this->subcategoriasRepository = $subcategoriasRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/subcategorias",
     *      summary="Get a listing of the Subcategorias.",
     *      tags={"Subcategorias"},
     *      description="Get all Subcategorias",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Subcategorias")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $subcategorias = $this->subcategoriasRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(SubcategoriasResource::collection($subcategorias), 'Subcategorias retrieved successfully');
    }

    /**
     * @param CreateSubcategoriasAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/subcategorias",
     *      summary="Store a newly created Subcategorias in storage",
     *      tags={"Subcategorias"},
     *      description="Store Subcategorias",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Subcategorias that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Subcategorias")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Subcategorias"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSubcategoriasAPIRequest $request)
    {
        $input = $request->all();

        $subcategorias = $this->subcategoriasRepository->create($input);

        return $this->sendResponse(new SubcategoriasResource($subcategorias), 'Subcategorias saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/subcategorias/{id}",
     *      summary="Display the specified Subcategorias",
     *      tags={"Subcategorias"},
     *      description="Get Subcategorias",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Subcategorias",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Subcategorias"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Subcategorias $subcategorias */
        $subcategorias = $this->subcategoriasRepository->find($id);

        if (empty($subcategorias)) {
            return $this->sendError('Subcategorias not found');
        }

        return $this->sendResponse(new SubcategoriasResource($subcategorias), 'Subcategorias retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSubcategoriasAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/subcategorias/{id}",
     *      summary="Update the specified Subcategorias in storage",
     *      tags={"Subcategorias"},
     *      description="Update Subcategorias",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Subcategorias",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Subcategorias that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Subcategorias")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Subcategorias"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSubcategoriasAPIRequest $request)
    {
        $input = $request->all();

        /** @var Subcategorias $subcategorias */
        $subcategorias = $this->subcategoriasRepository->find($id);

        if (empty($subcategorias)) {
            return $this->sendError('Subcategorias not found');
        }

        $subcategorias = $this->subcategoriasRepository->update($input, $id);

        return $this->sendResponse(new SubcategoriasResource($subcategorias), 'Subcategorias updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/subcategorias/{id}",
     *      summary="Remove the specified Subcategorias from storage",
     *      tags={"Subcategorias"},
     *      description="Delete Subcategorias",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Subcategorias",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Subcategorias $subcategorias */
        $subcategorias = $this->subcategoriasRepository->find($id);

        if (empty($subcategorias)) {
            return $this->sendError('Subcategorias not found');
        }

        $subcategorias->delete();

        return $this->sendSuccess('Subcategorias deleted successfully');
    }
}
