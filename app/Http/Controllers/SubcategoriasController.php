<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSubcategoriasRequest;
use App\Http\Requests\UpdateSubcategoriasRequest;
use App\Repositories\SubcategoriasRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class SubcategoriasController extends AppBaseController
{
    /** @var  SubcategoriasRepository */
    private $subcategoriasRepository;

    public function __construct(SubcategoriasRepository $subcategoriasRepo)
    {
        $this->subcategoriasRepository = $subcategoriasRepo;
    }

    /**
     * Display a listing of the Subcategorias.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $subcategorias = $this->subcategoriasRepository->all();

        return view('subcategorias.index')
            ->with('subcategorias', $subcategorias);
    }

    /**
     * Show the form for creating a new Subcategorias.
     *
     * @return Response
     */
    public function create()
    {
        return view('subcategorias.create');
    }

    /**
     * Store a newly created Subcategorias in storage.
     *
     * @param CreateSubcategoriasRequest $request
     *
     * @return Response
     */
    public function store(CreateSubcategoriasRequest $request)
    {
        $input = $request->all();

        $subcategorias = $this->subcategoriasRepository->create($input);

        Flash::success('Subcategorias saved successfully.');

        return redirect(route('subcategorias.index'));
    }

    /**
     * Display the specified Subcategorias.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $subcategorias = $this->subcategoriasRepository->find($id);

        if (empty($subcategorias)) {
            Flash::error('Subcategorias not found');

            return redirect(route('subcategorias.index'));
        }

        return view('subcategorias.show')->with('subcategorias', $subcategorias);
    }

    /**
     * Show the form for editing the specified Subcategorias.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $subcategorias = $this->subcategoriasRepository->find($id);

        if (empty($subcategorias)) {
            Flash::error('Subcategorias not found');

            return redirect(route('subcategorias.index'));
        }

        return view('subcategorias.edit')->with('subcategorias', $subcategorias);
    }

    /**
     * Update the specified Subcategorias in storage.
     *
     * @param int $id
     * @param UpdateSubcategoriasRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSubcategoriasRequest $request)
    {
        $subcategorias = $this->subcategoriasRepository->find($id);

        if (empty($subcategorias)) {
            Flash::error('Subcategorias not found');

            return redirect(route('subcategorias.index'));
        }

        $subcategorias = $this->subcategoriasRepository->update($request->all(), $id);

        Flash::success('Subcategorias updated successfully.');

        return redirect(route('subcategorias.index'));
    }

    /**
     * Remove the specified Subcategorias from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $subcategorias = $this->subcategoriasRepository->find($id);

        if (empty($subcategorias)) {
            Flash::error('Subcategorias not found');

            return redirect(route('subcategorias.index'));
        }

        $this->subcategoriasRepository->delete($id);

        Flash::success('Subcategorias deleted successfully.');

        return redirect(route('subcategorias.index'));
    }
}
