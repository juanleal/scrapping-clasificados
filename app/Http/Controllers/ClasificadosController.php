<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateClasificadosRequest;
use App\Http\Requests\UpdateClasificadosRequest;
use App\Repositories\ClasificadosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ClasificadosController extends AppBaseController
{
    /** @var  ClasificadosRepository */
    private $clasificadosRepository;

    public function __construct(ClasificadosRepository $clasificadosRepo)
    {
        $this->clasificadosRepository = $clasificadosRepo;
    }

    /**
     * Display a listing of the Clasificados.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $clasificados = $this->clasificadosRepository->paginate(50);

        return view('clasificados.index')
            ->with('clasificados', $clasificados);
    }

    /**
     * Show the form for creating a new Clasificados.
     *
     * @return Response
     */
    public function create()
    {
        return view('clasificados.create');
    }

    /**
     * Store a newly created Clasificados in storage.
     *
     * @param CreateClasificadosRequest $request
     *
     * @return Response
     */
    public function store(CreateClasificadosRequest $request)
    {
        $input = $request->all();

        $clasificados = $this->clasificadosRepository->create($input);

        Flash::success('Clasificados saved successfully.');

        return redirect(route('clasificados.index'));
    }

    /**
     * Display the specified Clasificados.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $clasificados = $this->clasificadosRepository->find($id);

        if (empty($clasificados)) {
            Flash::error('Clasificados not found');

            return redirect(route('clasificados.index'));
        }

        return view('clasificados.show')->with('clasificados', $clasificados);
    }

    /**
     * Show the form for editing the specified Clasificados.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $clasificados = $this->clasificadosRepository->find($id);

        if (empty($clasificados)) {
            Flash::error('Clasificados not found');

            return redirect(route('clasificados.index'));
        }

        return view('clasificados.edit')->with('clasificados', $clasificados);
    }

    /**
     * Update the specified Clasificados in storage.
     *
     * @param int $id
     * @param UpdateClasificadosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateClasificadosRequest $request)
    {
        $clasificados = $this->clasificadosRepository->find($id);

        if (empty($clasificados)) {
            Flash::error('Clasificados not found');

            return redirect(route('clasificados.index'));
        }

        $clasificados = $this->clasificadosRepository->update($request->all(), $id);

        Flash::success('Clasificados updated successfully.');

        return redirect(route('clasificados.index'));
    }

    /**
     * Remove the specified Clasificados from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $clasificados = $this->clasificadosRepository->find($id);

        if (empty($clasificados)) {
            Flash::error('Clasificados not found');

            return redirect(route('clasificados.index'));
        }

        $this->clasificadosRepository->delete($id);

        Flash::success('Clasificados deleted successfully.');

        return redirect(route('clasificados.index'));
    }
}
