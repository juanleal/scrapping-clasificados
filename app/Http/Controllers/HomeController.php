<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\ScrappersResource;
use Goutte\Client;
use App\Http\ScrapperInterface;
use App\Models\Categorias;
use App\Models\Subcategorias;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $clasificados = [];
        $scraper = new ScrappersResource(new Client());
        $url = "https://www.milanuncios.com/";
        if ($request->has('subcategorias_id') && $request->subcategorias_id != null) {
            $subcategoria = Subcategorias::find($request->subcategorias_id);
            if ($subcategoria->categorias->id == $request->categorias_id) {
                $url .= $subcategoria->name . "/?fromSearch=1";
                $dataScrapper = new ScrapperInterface($url, $subcategoria->filters, $subcategoria->main_filter);
            }
        } elseif ($request->has('categorias_id') && $request->categorias_id != null) {
            $categoria = Categorias::find($request->categorias_id);
            $url .= $categoria->name . "/?fromSearch=1";
            $dataScrapper = new ScrapperInterface($url, $categoria->filters, $categoria->main_filter);
        }
        if (isset($dataScrapper)) {
            $dataScrapper->categorias_id = $request->categorias_id;
            $dataScrapper->subcategorias_id = $request->subcategorias_id;
            $dataScrapper->save = $request->has('save') ? true : false;
            $clasificados = $scraper->handle($dataScrapper);
        }
        return view('home')->with(['clasificados' => $clasificados]);
    }
}
