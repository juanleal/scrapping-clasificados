<?php

namespace App\Http\Resources;

use Goutte\Client as GoutteClient;
use App\Models\Clasificados;

/**
 * Class ScrappersResource
 *
 * handles and process scraping using specific link
 * first we work on the main filter expression which is the
 * the container of the items, then using annonymous callback
 * on the filter function we iterate and save the results
 * into the clasificados table
 *
 * @package App\Http\Resources
 */
class ScrappersResource
{
    protected $client;
    public $results = [];
    public $savedItems = 0;
    public $status = 1;

    public function __construct(GoutteClient $client)
    {
        $this->client = $client;
    }
    public function handle($linkObj)
    {
        try {
            $crawler = $this->client->request('GET', $linkObj->url);
            $translateExpre = $this->translateCSSExpression($linkObj->css_expression);
            if (isset($translateExpre['title'])) {
                $data = [];
                // filter
                $crawler->filter($linkObj->main_filter_selector)->each(function ($node) use ($translateExpre, &$data, $linkObj) {
                    // using the $node var we can access sub elements deep the tree
                    foreach ($translateExpre as $key => $val) {
                        if ($node->filter($val['selector'])->count() > 0) {
                            if ($val['is_attribute'] == false) {
                                $data[$key][] = preg_replace("#\n|'|\"#", '', $node->filter($val['selector'])->text());
                            } else {
                                if ($key == 'link') {
                                    $item_link = $node->filter($val['selector'])->attr($val['attr']);
                                    //protocol: absolute url
                                    if (strpos($item_link, '://') !== false) {
                                        $data[$key][] = $item_link;
                                    } else {
                                        //leading '/': absolute to domain name (half relative)
                                        $url_info = parse_url($linkObj->url);
                                        $data[$key][] = $url_info['scheme'] . '://'. $url_info['host'] . $item_link;
                                    }
                                } else {
                                    $data[$key][] = $node->filter($val['selector'])->attr($val['attr']);
                                }
                            }
                        }
                    }
                    $data['categorias_id'][] = $linkObj->categorias_id;
                    $data['subcategorias_id'][] = $linkObj->subcategorias_id;
                });
                if ($linkObj->save) {
                    $this->save($data);
                }
                $this->results = $data;
            }
        } catch (\Exception $ex) {
            $this->status = $ex->getMessage();
            dd(['Error' => $ex]);
        }
        return $this->results;
    }

    protected function save($data)
    {
        if (isset($data['title'])) {
            foreach ($data['title'] as $k => $val) {
                $checkExist = Clasificados::where('ad_id', $data['ad_id'][$k])->first();
                if (!isset($checkExist->id)) {
                    $clasificado = new Clasificados();
                    $clasificado->title = $val;
                    $clasificado->ad_id = isset($data['ad_id'][$k]) ? $data['ad_id'][$k] : "";
                    $clasificado->content = isset($data['content'][$k]) ? $data['content'][$k] : "";
                    $clasificado->image_url = isset($data['image_url'][$k]) ? $data['image_url'][$k] : "";
                    $clasificado->link = isset($data['link'][$k]) ? $data['link'][$k] : "";
                    $clasificado->categorias_id = $data['categorias_id'][$k];
                    $clasificado->subcategorias_id = $data['subcategorias_id'][$k];
                    $clasificado->save();
                    $this->savedItems++;
                }
            }
        }
    }

    /**
     * translateCSSExpression
     *
     * translate the css expression into corresponding fields and sub selectors
     *
     * @param $expression
     * @return array
     */
    protected function translateCSSExpression($expression)
    {
        $exprArray = explode("||", $expression);
        // try to match split that expression into pieces
        $regex = '/(.*?)\[(.*)\]/m';
        $fields = [];
        foreach ($exprArray as $subExpr) {
            preg_match($regex, $subExpr, $matches);
            if (isset($matches[1]) && isset($matches[2])) {
                $is_attribute = false;
                $selector = $matches[2];
                $attr = "";
                // if this condition meets then this is attribute like img[src] or a[href]
                if (strpos($selector, "[") !== false && strpos($selector, "]") !== false) {
                    $is_attribute = true;
                    preg_match($regex, $matches[2], $matches_attr);
                    $selector = $matches_attr[1];
                    $attr = $matches_attr[2];
                }
                $fields[$matches[1]] = ['field' => $matches[1], 'is_attribute' => $is_attribute, 'selector' => $selector, 'attr' => $attr];
            }
        }
        return $fields;
    }
}
