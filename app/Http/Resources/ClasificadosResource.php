<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClasificadosResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'ad_id' => $this->ad_id,
            'title' => $this->title,
            'content' => $this->content,
            'image_url' => $this->image_url,
            'link' => $this->link,
            'categoria' => $this->categorias,
            'subcategoria' => $this->subcategorias,
            'comments' => $this->comments,
        ];
    }
}
