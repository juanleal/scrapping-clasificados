<?php

namespace App\Http;

class ScrapperInterface
{
    public $url;
    public $css_expression;
    public $main_filter_selector;
    public $categorias_id;
    public $subcategorias_id;
    public $save;

    /**
     * Create a new instance.
     *
     * @return void
     */
    public function __construct($url, $css_expression, $main_filter_selector)
    {
        $this->url = $url;
        $this->css_expression = $css_expression;
        $this->main_filter_selector = $main_filter_selector;
    }
}
